const Cita=require('../models/cita.models')
const Medico=require('../models/medico.model.js')


exports.guardar=(req,res)=>{
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    const cita = new Cita({
        Nombres:req.body.Nombres,
        Apellidos:req.body.Apellidos,
        Telefono:req.body.Telefono,
        Direccion:req.body.Direccion,
        Fecha:req.body.Fecha,
        Medico:req.body.Medico
        });
    cita.save()
    .then(data => {
        res.status(200).send(data)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al crear el registro"
        });
    });
};

exports.consultar = (req,res)=>{
    Cita.find({}).populate('Medico')
    .then(Citas => {
        res.status(200).send(Citas);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error"
        });
    });
};

exports.consultarone=(req,res)=>{
   Cita.findById(req.params.id)
   .then(cita =>{
       if(!cita){
           return res.status(404).send({
               message:"Producto no encontrado" + req.params.id
           });
       }
       res.status(200).send(cita);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al mostrar el registro"
        });
   })
};
exports.modificar=(req,res)=>{
    const body= req.body;
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    Cita.findByIdAndUpdate(req.params.id, body,{new : true},{
        Nombres:req.body.Nombres,
        Apellidos:req.body.Apellidos,
        Telefono:req.body.Telefono,
        Direccion:req.body.Direccion,
        Fecha:req.body.Fecha,
        Medico:req.body.Medico
        },{new:true}).then(cita=>{
        if(!cita){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send(cita);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al actualizar el registro" + req.params.id
        });
   })
};
exports.eliminar=(req,res)=>{
    Cita.findByIdAndRemove(req.params.id)
    .then(cita=>{
        if(!cita){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send({
            message:"Producto eliminado con exito"
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al eliminar el registro" + req.params.id
        });
   });
};

exports.listar_medico=(req,res)=>{
    Medico.find({})
        .then(citas => {
    res.status(200).send(citas);
}).catch(err => {
    res.status(500).send({
        message: err.message || "ocurrio un error"
    });
});
};