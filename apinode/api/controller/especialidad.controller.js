const Especialidad=require('../models/especialidad.model.js')

exports.create=(req,res)=>{
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    const especia = new Especialidad({
        Nombre:req.body.Nombre
        });
    especia.save()
    .then(data => {
        res.status(200).send(data)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al crear el registro"
        });
    });
};

exports.findAll=(req,res)=>{
    Especialidad.find()
    .then(especials => {
        res.status(200).send(especials);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error"
        });
    });
};

exports.findOne=(req,res)=>{
    Especialidad.findById(req.params.id)
   .then(especia =>{
       if(!especia){
           return res.status(404).send({
               message:"Producto no encontrado" + req.params.id
           });
       }
       res.status(200).send(especia);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al mostrar el registro"
        });
   })
};
exports.update=(req,res)=>{
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    Especialidad.findByIdAndUpdate(req.params.id,{

        Nombre:req.body.Nombre
        },{new:true}).then(especia=>{
        if(!especia){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send(especia);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al actualizar el registro" + req.params.id
        });
   })
};
exports.edelete=(req,res)=>{
    Especialidad.findByIdAndRemove(req.params.id)
    .then(especia=>{
        if(!especia){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send({
            message:"Producto eliminado con exito"
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al eliminar el registro" + req.params.id
        });
   });
};