const Medico=require('../models/medico.model.js')
const Especialidad=require('../models/especialidad.model.js')


exports.guardar=(req,res)=>{
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    const medico = new Medico({
        Nombres:req.body.Nombres,
        Apellidos:req.body.Apellidos,
        Correo:req.body.Correo,
        Especialidad:req.body.Especialidad
        });
    medico.save()
    .then(data => {
        res.status(200).send(data)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al crear el registro"
        });
    });
};

exports.consultar = (req,res)=>{
    Medico.find({}).populate('Especialidad')
    .then(Medics => {
        res.status(200).send(Medics);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error"
        });
    });
};

exports.consultarone=(req,res)=>{
   Medico.findById(req.params.id)
   .then(medico =>{
       if(!medico){
           return res.status(404).send({
               message:"Producto no encontrado" + req.params.id
           });
       }
       res.status(200).send(medico);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al mostrar el registro"
        });
   })
};
exports.modificar=(req,res)=>{
    const body= req.body;
    if(Object.keys(req.body).length===0){
        return res.status(400).send({
            message:"Los datos no pueden estar vacios"
        });
    }
    Medico.findByIdAndUpdate(req.params.id, body,{new : true},{
        Nombres:req.body.Nombres,
        Apellidos:req.body.Apellidos,
        Correo:req.body.Correo,
        Especialidad:req.body.Especialidad
        },{new:true}).then(medico=>{
        if(!medico){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send(medico);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al actualizar el registro" + req.params.id
        });
   })
};
exports.eliminar=(req,res)=>{
    Medico.findByIdAndRemove(req.params.id)
    .then(medico=>{
        if(!medico){
            return res.status(404).send({
                message:"Producto no encontrado por id" + req.params.id
            });
        }
        res.status(200).send({
            message:"Producto eliminado con exito"
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "ocurrio un error al eliminar el registro" + req.params.id
        });
   });
};

exports.listar_especialidad=(req,res)=>{
    Especialidad.find({})
        .then(Medics => {
    res.status(200).send(Medics);
}).catch(err => {
    res.status(500).send({
        message: err.message || "ocurrio un error"
    });
});
};