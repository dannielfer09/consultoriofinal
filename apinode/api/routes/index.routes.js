const express = require('express');

var app = express();

app.use('/api',require('./medico.routes'));
app.use('/api',require('./especialidad.routes'));
app.use('/api',require('./cita.routes'));
module.exports = app;
