const express = require ('express');
const {consultar,consultarone,guardar,modificar,eliminar,listar_especialidad} = require('../controller/medico.controller')

const router = express.Router();

const cors = require('cors');
const allowedOrigins = [
    'capacitor://localhost',
    'ionic://localhost',
    'http://localhost',
    'http://localhost:8101',
    'http://localhost:8100',
  ];
  
  // Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
  const corsOptions = {
    origin: (origin, callback) => {
      if (allowedOrigins.includes(origin) || !origin) {
        callback(null, true);
      } else {
        callback(new Error('Origin not allowed by CORS'));
      }
    },
  };
 router.options('*', cors(corsOptions));

  // Enable preflight requests for all routes

router.get('/medico',cors(corsOptions),consultar);
router.get('/medico/especialidad',cors(corsOptions),listar_especialidad);
router.post('/medico',cors(corsOptions),guardar);
router.get('/medico/:id',cors(corsOptions),consultarone);
router.put('/medico/:id',cors(corsOptions),modificar);
router.delete('/medico/:id',cors(corsOptions),eliminar);
module.exports = router;