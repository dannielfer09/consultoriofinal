
const express = require ('express');
const {findAll,create,findOne,update,edelete} = require('../controller/especialidad.controller')

const router = express.Router();
const cors = require('cors');
const allowedOrigins = [
    'capacitor://localhost',
    'ionic://localhost',
    'http://localhost',
    'http://localhost:8101',
    'http://localhost:8100',
  ];
  
  // Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
  const corsOptions = {
    origin: (origin, callback) => {
      if (allowedOrigins.includes(origin) || !origin) {
        callback(null, true);
      } else {
        callback(new Error('Origin not allowed by CORS'));
      }
    },
  };
 router.options('*', cors(corsOptions));

router.get('/especialidad',cors(corsOptions),findAll);
router.post('/especialidad',cors(corsOptions),create);
router.get('/especialidad/:id',cors(corsOptions),findOne);
router.put('/especialidad/:id',cors(corsOptions),update);
router.delete('/especialidad/:id',cors(corsOptions),edelete);
module.exports = router;
                 