const  mongoose=require('mongoose');

const Schema = mongoose.Schema;

const EspecialidadSchema=new Schema({
    Nombre:{
        type:String,
        index:true,
        unique:true,
        require:true,
        trim:true,
        minlength:4
    }
},{
    timestamps:true
});

module.exports=mongoose.model('Especialidad',EspecialidadSchema,'especialidad')