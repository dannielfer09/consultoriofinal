const  mongoose=require('mongoose');

const Schema  = mongoose.Schema;

const MedicoSchema=new Schema({
    Nombres:{
        type:String,
        index:true,
        unique:true,
        require:true,
        trim:true,
        minlength:4
    },
    Apellidos:{
        type:String,
        index:true,
        require:true,
        trim:true,
        minlength:4
    },
    Correo:{
        type:String,
        require:true,
        trim:true,
        minlength:4
    },
    Especialidad:[{
        type: Schema.Types.ObjectId, ref:'Especialidad', autopopulate:true
    }]

},{
    timestamps:true
});

// MedicSchema.plugin(require('mongoose-autopopulate'));

module.exports=mongoose.model('Medico',MedicoSchema,'medico')