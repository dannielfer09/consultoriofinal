const  mongoose=require('mongoose');

const Schema  = mongoose.Schema;

const CitaSchema=new Schema({
    Nombres:{
        type:String,
        index:true,
        unique:true,
        require:true,
        trim:true,
        minlength:4
    },
    Apellidos:{
        type:String,
        index:true,
        require:true,
        trim:true,
        minlength:4
    },
    Telefono:{
        type:Number,
        require:true,
        trim:true,
        minlength:4
    },
    Direccion:{
        type:String,
        require:true,
        trim:true,
        minlength:4
    },
    Fecha:{
        type: String,
        require:false,
        trim:true,
        minlength:4
    },
    Medico:[{
        type: Schema.Types.ObjectId, ref:'Medico', autopopulate:true
    }]

},{
    timestamps:true
});

// MedicSchema.plugin(require('mongoose-autopopulate'));

module.exports=mongoose.model('Cita',CitaSchema,'cita')