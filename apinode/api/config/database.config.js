//module.exports={
//url:'mongodb+srv://seminario:seminario@cluster0.mb7tf.mongodb.net/simple?retryWrites=true&w=majority'
//}
//
const mongoose = require('mongoose');

const URI = process.env.MONGODB_URI ? 
process.env.MONGODB_URI: 'mongodb://localhost/Consultorio2';

mongoose.connect(URI,{
    useNewUrlParser: true,
    useUnifiedTopology:true,
});

const connection=mongoose.connection;

connection.once('open', () =>{
console.log('Base de datos conectada');
});