
//Importar modulos
const express=require('express');
const bodyParser=require('body-parser');
const cors = require('cors');
const app=express();
  
  // Enable preflight requests for all routes

require('./api/config/database.config'),

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.set('port', process.env.PORT || 3000);

app.use(require("./api/routes/index.routes"));

async function main(){
    await  app.listen(app.get('port'));
   console.log('Puerto',app.get('port'));
  }
  main();