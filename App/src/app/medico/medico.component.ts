import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { CrearmedicoComponent } from '../home/crearmedico/crearmedico.component';
import { Medico } from '../interface/medico';
import { MedicoService } from '../services/medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.scss'],
})
export class MedicoComponent implements OnInit {
  medicos : Medico[] = [];
//
constructor(private medicosService : MedicoService,
  private modalCtrl:ModalController,
  private toasCrtl:ToastController) 
{}
ngOnInit() {
    this.getAllMedicos();
}
getAllMedicos(){
  this.medicosService.getAllMedicos()
  .subscribe(medicos=>{
    this.medicos=medicos;
    //console.log(medicos);
  })
}
async crearmedico(id? : string) {
  const modal = await this.modalCtrl.create({
    component: CrearmedicoComponent,
    componentProps:{id},
   
  });
  return await modal.present();
}
delete(id:string, index:number) {
  this.medicosService.deleteMedico(id)
  .subscribe(()=>{
    this.medicos.splice(index,1);
    this.presentToast('El medico ha sido eliiado con exito');
  });
}
async presentToast(message: string){
  const toast = await this.toasCrtl.create({
    message,
    duration:3000
  });
  await toast.present();
}
}

