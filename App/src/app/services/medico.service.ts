import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Medico } from '../interface/medico';
import { Especialidad } from '../interface/especialidad';
import { Cita } from '../interface/cita';
@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  private api = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getAllMedicos(){
    const path = `${this.api}/api/medico`;
    return this.http.get<Medico[]>(path);
  }
  getAllEspecialidad(){
    const path = `${this.api}/api/especialidad`;
    return this.http.get<Medico[]>(path);
  }
  createMedico(medico:Medico){
    const path = `${this.api}/api/medico`;
    return this.http.post<Medico>(path,medico);
  }
  updateMedico(medico:Medico){
    const path = `${this.api}/api/medico/${medico._id}`;
    return this.http.put<Medico>(path,medico);
  }
  deleteMedico(id:string){
    const path = `${this.api}/api/medico/${id}`;
    return this.http.delete(path);
  }
  getMedicoByID(id:string){
    const path = `${this.api}/api/medico/${id}`;
    return this.http.get(path);
  }
  getCitaByID(id:string){
    const path = `${this.api}/api/cita/${id}`;
    return this.http.get(path);
  }
  getAllCita(){
    const path = `${this.api}/api/cita`;
    return this.http.get<Cita[]>(path);
  } 
  createCita(cita:Cita){
    const path = `${this.api}/api/cita`;
    return this.http.post<Cita>(path,cita);
  } 
  deleteCita(id:string){
    const path = `${this.api}/api/cita/${id}`;
    return this.http.delete(path);
  }
  updateCita(cita:Cita){
    const path = `${this.api}/api/cita/${cita._id}`;
    return this.http.put<Cita>(path,cita);
  }
  getMeByID(id:string){
    const path = `${this.api}/api/medico/especialidad${id}`;
    return this.http.get(path);
  }
}
