import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { CrearmedicoComponent } from '../home/crearmedico/crearmedico.component';
import { defaultData } from '../home/utils/forms';
import { Especialidad } from '../interface/especialidad';
import { Medico } from '../interface/medico';
import { MedicoService } from '../services/medico.service';

@Component({
  selector: 'app-listarmedicos',
  templateUrl: './listarmedicos.component.html',
  styleUrls: ['./listarmedicos.component.scss'],
})
export class ListarmedicosComponent implements OnInit {
  @Input() id:string;
  form: FormGroup;
  medicos : Medico[] = [];
  especialidad : Especialidad[] = [];
  constructor(private listamedicoService: MedicoService,
    private modalCtrl: ModalController,private toasCrtl:ToastController,
    private formBuilder:FormBuilder,
  ) { }

  ngOnInit() {
    this.getAllMedicos();
    this.form = this.formBuilder.group({
      Nombres: defaultData(),
      Apellidos:defaultData(),
      Correo:defaultData(),
      Especialidad:defaultData(),
    });
    this.listamedicoService.getAllEspecialidad()
    .subscribe(especialidad =>{
      this.especialidad=especialidad;
    },error=>
      console.log(error));
      
      
      if(this.id){
        this.listamedicoService.getMedicoByID(this.id)
        .subscribe(data=>{
          Object.keys(this.form.value).forEach(key=>{
            this.form.patchValue({[`${key}`]:data[`${key}`]})
          });
        });
      }
  }

  getAllMedicos(){
    this.listamedicoService.getAllMedicos()
    .subscribe(medicos=>{
      this.medicos=medicos;
      //console.log(medicos);
    })
  }
  closemodal(): void{
    this.modalCtrl.dismiss();
  
  }
  delete(id:string, index:number) {
    this.listamedicoService.deleteMedico(id)
    .subscribe(()=>{
      this.medicos.splice(index,1);
      this.presentToast('El medico ha sido eliminado con exito');
    });
  }
  async presentToast(message: string){
    const toast = await this.toasCrtl.create({
      message,
      duration:3000
    });
    await toast.present();
  }

  save(): void{
    if(!this.id){
      this.listamedicoService.createMedico(this.form.value)
        .subscribe(() => {
          console.log("usuario registrado")
        },error => console.log(error),
        );
      } else {
        this.listamedicoService.updateMedico({_id:this.id, ...this.form.value})
        .subscribe(()=>{
          console.log("usuario actualizado");
        },error => console.log(error),
        );
      }
  }
  async crearmedico(id? : string) {
    const modal = await this.modalCtrl.create({
      component: CrearmedicoComponent,
      componentProps:{id},
     
    });
    return await modal.present();
  }
}
