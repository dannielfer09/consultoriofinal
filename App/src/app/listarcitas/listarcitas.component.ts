import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { CitaComponent } from '../cita/cita.component';
import { defaultData } from '../home/utils/forms';
import { Cita } from '../interface/cita';
import { Medico } from '../interface/medico';
import { MedicoService } from '../services/medico.service';
import { format, parseISO } from 'date-fns';
import {Router} from  '@angular/router'

@Component({
  selector: 'app-listarcitas',
  templateUrl: './listarcitas.component.html',
  styleUrls: ['./listarcitas.component.scss'],
})
export class ListarcitasComponent implements OnInit {
  @Input() id:string;
form: FormGroup;
cita : Cita[] = [];
medico : Medico[] = [];
dateValue2 = '';
  constructor(private listacitaService: MedicoService,
    private modalCtrl: ModalController,private toasCrtl:ToastController,
    private formBuilder:FormBuilder,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getAllCita();
     this.form = this.formBuilder.group({
       Nombres: defaultData(),
       Apellidos:defaultData(),
       Telefono:defaultData(),
       Direccion:defaultData(),
       Fecha:this.dateValue2,
       Medico:defaultData(),
     });
    this.listacitaService.getAllMedicos()
    .subscribe(medico =>{
      this.medico=medico;
    },error=>
      console.log(error));
      if(this.id){
        this.listacitaService.getCitaByID(this.id)
        .subscribe(data=>{
          Object.keys(this.form.value).forEach(key=>{
            this.form.patchValue({[`${key}`]:data[`${key}`]})
          });
        });
      }

  }
  getAllCita(){
    this.listacitaService.getAllCita()
    .subscribe(citas=>{
      this.cita=citas;
      //console.log(medicos);
    })
  }
  closemodal(): void{
    this.modalCtrl.dismiss();
    
  }

  delete(id:string, index:number) {
    this.listacitaService.deleteCita(id)
    .subscribe(()=>{
      this.cita.splice(index,1);
      this.presentToast('La cita ha sido eliminada con exito');
    
    });
  }
  async presentToast(message: string){
    const toast = await this.toasCrtl.create({
      message,
      duration:3000
      
    });
    await toast.present();
  }
  save(): void{
    if(!this.id){
      this.listacitaService.createCita(this.form.value)
        .subscribe(() => {
          this.router.navigateByUrl('/home')
          console.log("usuario registrado")
        },error => console.log(error),
        );
      } else {
        this.listacitaService.updateCita({_id:this.id, ...this.form.value})
        .subscribe(()=>{
          console.log("usuario actualizado");
        },error => console.log(error),
        );
      }
  }
  async crearcita(id? : string) {
    const modal = await this.modalCtrl.create({
      component:CitaComponent,
      componentProps:{id},
     
    });
    return await modal.present();
  }
  formatDate(value: string) {
  return format(parseISO(value), 'MMM dd yyyy  HH mm');
}
}
