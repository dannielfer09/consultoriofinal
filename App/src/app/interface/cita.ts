export interface Cita {
  
    _id?:string;
    Nombres: string;
    Apellidos: string;
    Telefono: number;
    Direccion: string;
    Fecha: string;
    Medico:string;

}
