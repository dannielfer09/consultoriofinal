import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IonDatetime, ModalController } from '@ionic/angular';
import { defaultData } from '../home/utils/forms';
import { Medico } from '../interface/medico';
import { MedicoService } from '../services/medico.service';
import { format, parseISO } from 'date-fns';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.scss'],
})
export class CitaComponent implements OnInit {
  @ViewChild(IonDatetime, { static: true }) datetime: IonDatetime;
  @Input() id:string;
medico : Medico[] = [];
form: FormGroup;
dateValue2 = '';
constructor(private citaService : MedicoService,
  private modalCtrl: ModalController,
  private formBuilder:FormBuilder,
  public router: Router,) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      Nombres: defaultData(),
      Apellidos:defaultData(),
      Telefono:defaultData(),
      Direccion:defaultData(),
      Fecha:this.dateValue2,
      Medico:defaultData(),
    });
    this.citaService.getAllMedicos()
    .subscribe(medico =>{
      this.medico=medico;
    },error=>
      console.log(error));
      if(this.id){
        this.citaService.getCitaByID(this.id)
        .subscribe(data=>{
          Object.keys(this.form.value).forEach(key=>{
            this.form.patchValue({[`${key}`]:data[`${key}`]});
          });
        });
      }
  }
  closemodal(): void{
    this.modalCtrl.dismiss();
  }
  //confirmarfecha(event:CustomEvent){
  //  console.log(event.detail.value);
  //}
  save(): void{
    if(!this.id){
      this.citaService.createCita(this.form.value)
        .subscribe(() => {
          this.router.navigateByUrl('/')
          console.log("usuario registrado")
        },error => console.log(error),
        );
      } else {
        this.citaService.updateMedico({_id:this.id, ...this.form.value})
        .subscribe(()=>{
          console.log("usuario actualizado");
        },error => console.log(error),
        );
      }
  }
  formatDate(value: string) {
    return format(parseISO(value), 'MMM dd yyyy  HH mm');
  }

}
