import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { HomePageRoutingModule } from './home-routing.module';
import { CrearmedicoComponent } from './crearmedico/crearmedico.component';
import { Homepage } from './home.page';
import { CitaComponent } from '../cita/cita.component';
import { ListarmedicosComponent } from '../listarmedicos/listarmedicos.component';
import { ListarcitasComponent } from '../listarcitas/listarcitas.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HomePageRoutingModule
  ],
  declarations: [Homepage,CrearmedicoComponent,CitaComponent,ListarmedicosComponent,ListarcitasComponent],
  entryComponents:[CrearmedicoComponent,CitaComponent,ListarmedicosComponent,ListarcitasComponent],
})

export class HomePageModule {}
