import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { CitaComponent } from '../cita/cita.component';
import { CrearmedicoComponent } from '../home/crearmedico/crearmedico.component';
import { Medico } from '../interface/medico';
import { ListarcitasComponent } from '../listarcitas/listarcitas.component';
import { ListarmedicosComponent } from '../listarmedicos/listarmedicos.component';
import { MedicoService } from '../services/medico.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class Homepage implements OnInit {
  medicos : Medico[] = [];
//
constructor(private medicosService : MedicoService,
  private modalCtrl:ModalController,
  private toasCrtl:ToastController,
 ) 
{}
ngOnInit() {
    
}
async crearmedico(id? : string) {
  const modal = await this.modalCtrl.create({
    component: CrearmedicoComponent,
    componentProps:{id},
   
  });
  return await modal.present();
}
async crearcita(id? : string) {
  const modal = await this.modalCtrl.create({
    component: CitaComponent,
    //componentProps:{id},
   
  });
  return await modal.present();
}
async listarmedico(id? : string) {
  const modal = await this.modalCtrl.create({
    component: ListarmedicosComponent,
    componentProps:{id},
   
  });
  return await modal.present();
}
async listarcita(id? : string) {
  const modal = await this.modalCtrl.create({
    component: ListarcitasComponent,
    componentProps:{id},
   
  });
  return await modal.present();
}
}

