import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Especialidad } from 'src/app/interface/especialidad';
import { MedicoService } from 'src/app/services/medico.service';
import { defaultData } from '../utils/forms';

@Component({
  selector: 'app-crearmedico',
  templateUrl: './crearmedico.component.html',
  styleUrls: ['./crearmedico.component.scss'],
})
export class CrearmedicoComponent implements OnInit {

  @Input() id:string;
  especialidad : Especialidad[] = [];
  form: FormGroup;
  constructor(private medicosService : MedicoService,
    private modalCtrl: ModalController,
    private formBuilder:FormBuilder) { }

  ngOnInit() {

    this.form = this.formBuilder.group({
      Nombres: defaultData(),
      Apellidos:defaultData(),
      Correo:defaultData(),
      Especialidad:defaultData(),
    });
    this.medicosService.getAllEspecialidad()
    .subscribe(especialidad =>{
      this.especialidad=especialidad;
    },error=>
      console.log(error));

      if(this.id){
        this.medicosService.getMedicoByID(this.id)
        .subscribe(data=>{
          Object.keys(this.form.value).forEach(key=>{
            this.form.patchValue({[`${key}`]:data[`${key}`]});
          });
        });
      }
  }
  
  closemodal(): void{
    this.modalCtrl.dismiss();
  }


  save(): void{
    if(!this.id){
      this.medicosService.createMedico(this.form.value)
        .subscribe(() => {
          console.log("usuario registrado")
        },error => console.log(error),
        );
      } else {
        this.medicosService.updateMedico({_id:this.id, ...this.form.value})
        .subscribe(()=>{
          console.log("usuario actualizado");
        },error => console.log(error),
        );

      }
  }
  
}
