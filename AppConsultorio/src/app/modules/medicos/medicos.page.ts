import { Medico } from '@core/models/Medico.model';
import { Component, OnInit } from '@angular/core';

import { MedicoService } from '@core/services/medico.service';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.page.html',
  styleUrls: ['./medicos.page.scss'],
})
export class MedicosPage implements OnInit {

  medicos: Medico []= []
  constructor(private medicoService:MedicoService) { }

  ngOnInit() {
    this.medicoService.getAllMedico().subscribe(data=>{
      console.log(data);
      //this.medicos = data;
    })
  }

}
