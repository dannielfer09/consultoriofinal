import { Injectable } from '@angular/core';
import { USER,USER2 } from '@core/constanst/index';
import { Especialidad } from '@core/models/Especialidad.model';
import { Medico } from '@core/models/Medico.model';
import { ApiService } from '@core/services/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicoService extends ApiService {

  public getAllMedico(): Observable<any>{
    return this.get(USER.BASEMEDICO);
  }
  public getAllEspecialidad(): Observable<any>{
    return this.get(`${USER.BASEMEDICO}/especialidad`);
  }
  /**
   * CrearMedico
   */
  public CrearMedico(medico:Medico):Observable<any> {
    return this.post(USER.BASEMEDICO,medico);
  }
   /**
    * updateMedico
    */
   public updateMedico(medico:Medico):Observable<any> {
     const {_id, ...data} =medico;
     return this.update(`${USER.BASEMEDICO}/${_id}`,data);
   }

   /**
    * deleteMedico
    */
   public deleteMedico(id:string): Observable<any>{
    return this.delete(`${USER.BASEMEDICO}/${id}`);
   }
}
