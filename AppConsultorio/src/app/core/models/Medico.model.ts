export interface Medico {
    _id?:string;
    Nombres: string;
    Apellidos: string;
    Correo: string;
    Especialidad: string;
}